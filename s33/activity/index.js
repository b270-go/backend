// 3

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(response => console.log(response))

// 4
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(response => {
    let titles = response.map(item => item.title);
    console.log(titles);
  });

// 5
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(response => console.log(response))

// 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(data => {
    const { title, completed } = data;
    console.log(`Title: ${title} | Status: ${completed ? 'Complete' : 'Incomplete'}`);
  });


// 7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Post new task",
		completed: true,
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));

// 8
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Put new task",
		completed: true,
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));

// 9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "update to do list",
		description: true,
		status: "completed",
		dateCompleted: "04/04/2004",
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));

// 10
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title",
		
	})
})
.then(response => response.json())
.then(json => console.log(json));

// 11
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: "completed",
		dateCompleted: "03/03/2003"
		
	})
})
.then(response => response.json())
.then(json => console.log(json));

// 12
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response));