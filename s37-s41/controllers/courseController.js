const Course= require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	let newCourse = new Course({
		name: req.body.name, 
		description: req.body.description,
		price: req.body.price,
		slots: req.body.slots
	})

	if(userData.isAdmin == true){
		return newCourse.save().then(course => {
			console.log(course)
			res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		})
	} else {
		res.status(403).send("Forbidden: Needs admin rights")
	}
	
}

// Retrieve all courses
//retrieve all courses

module.exports.getAllCourses = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        return Course.find({}).then(result => res.send(result));
    } else {
        return res.send(false);
    }

}

// Retrieve all active courses
module.exports.getAllActive = (req, res) => {

    return Course.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (req, res) => {

    console.log(req.params.courseId)

    return Course.findById(req.params.courseId).then(result => {
    	console.log(result);
    	return res.send(result);
    })
    .catch(error => {
    	console.log(error);
    	return res.send(error)
    })
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let updateCourse = {
			name: req.body.name, 
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}
		//{new:true} = returns the updated document
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new:true}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false)
	}
}


// For archiving a course
module.exports.archiveCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		if(userData.isAdmin) {
			let updateActivity = {
				isActive: req.body.isActive
			}
			
			return Course.findByIdAndUpdate(req.params.courseId, updateActivity).then(result => {
				console.log(result);
				res.send(true);
			})
			.catch(error => {
				console.log(error);
				res.send(false);
			})
		} else {
			return res.send(false)
		}
	}