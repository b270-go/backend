const express = require("express");
const router = express.Router()
const userController = require("../controllers/userController")
const auth = require("../auth")

// Route for checking if the user's email already exists in the database
// Invoke the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", userController.checkEmailExists)

router.post("/register", userController.registerUser)

router.post("/login", userController.loginUser);

// ACTIVITY

router.get("/details", auth.verify, userController.getProfile)

router.post("/enroll", auth.verify, userController.enroll);


module.exports = router; 