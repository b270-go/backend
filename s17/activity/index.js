/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function provideMetadata() {
		let fullName = prompt('Enter your name');
		let ageNumber = prompt('How old are you?');
		let fullAddress = prompt('Where do you live?');

		console.log('Hello ' + fullName);
		console.log('You are ' + ageNumber + ' years old.');
		console.log('You live in ' + fullAddress);
		alert('Thank you for your input!');
	}
	provideMetadata();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands() {
		console.log('1. The Beatles')
		console.log('2. Metallica')
		console.log('3. The Eagles')
		console.log('4. Eraserheads')
		console.log('5. Imagine Dragons')
	}

	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function firstFavoriteMovie() {
		console.log('1. Psycho');
		console.log('Rotten Tomatoes Rating: 96%');
	}

	firstFavoriteMovie();

	function secondFavoriteMovie() {
		console.log('2. John Wick 4');
		console.log('Rotten Tomatoes Rating: 94%');
	}

	secondFavoriteMovie();

	function thirdFavoriteMovie() {
		console.log('3. Avengers: Endgame');
		console.log('Rotten Tomatoes Rating: 94%');
	}

	thirdFavoriteMovie();

	function fourthFavoriteMovie() {
		console.log('4. Honor Thy Father');
		console.log('Rotten Tomatoes Rating: 99%');
	}

	fourthFavoriteMovie();
	function fifthFavoriteMovie() {
		console.log('5. On The Job');
		console.log('Rotten Tomatoes Rating: 99%');
	}

	fifthFavoriteMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: ")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);

