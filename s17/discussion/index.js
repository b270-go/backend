//console.log("Hello World")

// Functions
	//Functions in JS are lines/block of codes that can tell our device/application to perform a certain tak when called or invoked.

//Functions Declarations
	//functions statement - defines a function with a specified parameter
	/*Syntax:
	function functionName(){
		code block statement
	}	

		-functiom keyword - used to define a JS function
		-functionName - is the function's name.Functions are name to be able to used to called later in our code
		-function block ({}) - the statements which comprise the body of the function. Thisis where the codde to be executed is wriiten.

	*/

	function printName() {
		console.log("HI! My name is John.");
	};

	//Function invocation.
	printName();

	//declaredFunction(); - result to an error;index.js:26 Uncaught ReferenceError: declaredFunction is not defined
  	


	// Semicolons are used to seperate executable JS statements or codes.


// Function Declaration vs Function Expressions
	
	// 1. Function declaration
	declaredFunction(); //declared funcstions can be hoisted. As long as the function has been declared.

	function declaredFunction () {
		console.log("Hello World from declaredFunction()")
	}

	declaredFunction();

	// 2. Functions Expression

		// A function can also be store in a variable.
		// A function expression is anonymous function assigned to the variableFunction


		/*

		let variableName = function() {
				code block (statement);
		}

		*/
		//let n = 30; // this is how we initializa a variable

		//variableFunction(); this result an error

		let variableFunction = function() {
			console.log("Hello again Batch 270!")
		}

		variableFunction();

		//we invoke the function expression using its variable name not its function name
		let funcExpression = function funcName() {
			console.log("Hello from the other side.")
		}

		funcExpression();

		// You can ressign declared functions and function expressions to a new anonymous functions.

		declaredFunction = function () {
			console.log("updated declaredFunction")
		}

		funcExpression = function () {
			console.log("updated funcExpression");
		}

		declaredFunction();
		funcExpression();

		const constantFunc = function() {
			console.log("Initialized with const");
		}

		constantFunc();
		declaredFunction();

		/*

		Scope is the accessibility (visibility) of variables within our groups

		JavaSCript Variables has three types of scope:
		1. local/block scope
		2. global scope
		3. function scope
		*/

		{
			let localVar = 'Armando Perez';
			console.log(localVar);
		}

		// function global
		let globalVar = 'Mr. World';

		console.log(globalVar);
		// console.log(localVar);

		// function scope
		function showNames() {
			// fxn scoped variables
			var functionVar = "joe";
			const functionCost = "john";
			let functionLet = "jane";

			console.log(functionVar);
			console.log(functionCost);
			console.log(functionLet);
		}

		showNames();

	// Nested functions

	function myNewFunction(){
		let name = "jane";

		function nestedFunction() {
			let nestedName = "John";
			console.log(name);
		}
		nestedFunction();
	}
	myNewFunction();

	// Function and Global Scope Variables

	let globalName = "Alexandro";

	function myNewFunction2(){
		let nameInside = "Renz";
		console.log(globalName);
	}
	myNewFunction2();


	// Using alert()
		// alert('Hello World');

		// function showSampleAlert() {
		// 	alert("Hello, User!");
		// }

		// showSampleAlert();
		// console.log("")


	// Using prompt()
		let samplePrompt = prompt('Enter your name');
		console.log('hello,' + samplePrompt)

		function printWelcomeMessage() {
			let firstName = prompt('Enter your first name');
			let lastName = prompt('Enter your last name');

			console.log('hello ' + firstName + ' ' + lastName + '!');
			console.log('Welcome to my page!');
		}
		printWelcomeMessage();


//Function Naming Conventions
// 1. function names should be descriptive