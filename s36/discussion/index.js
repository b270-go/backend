// Setup the dependencies

const express = require("express")
const mongoose = require("mongoose")

const taskRoutes = require("./routes/taskRoutes")
// Server setup
const app = express(); 
const port = 3001;

// Middlewares 
app.use(express.json())
app.use(express.urlencoded({extended:true}))
// Add the task route 
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
 app.use("/tasks", taskRoutes);

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.oit04zm.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);



// Server listening
app.listen(port, () => console.log(`Server running at  ${port}`) );
