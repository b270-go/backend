const http = require("http");

// Creates a variable "port" to store the port number
const port = 3000

// Creates a variable "server" that stores the ouput of the "createServer()" method.
const server = http.createServer((request, response) => {

	// Accessing the 'greeting' route returns a message of 'Hello World'
	if (request.url == '/') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Hello, go to 'localhost:3000/login' ")
	} else if (request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("You are logged in! Welcome")
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("The page is not available")
	}
})

server.listen(port);

console.log(`Server is now accessble at localhost: ${port}.`)