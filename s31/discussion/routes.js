const http = require("http");

// Creates a variable "port" to store the port number
const port = 4000

// Creates a variable "server" that stores the ouput of the "createServer()" method.
const server = http.createServer((request, response) => {

	// Accessing the 'greeting' route returns a message of 'Hello World'
	if (request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Hello batch 270")

	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("this is the homepage")
	} else if (request.url == '/profile') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("this is the profile page")
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("page not available")
	}
})

server.listen(port);

console.log('Server is now accessble at localhost: ${port}.')