const getCube = num => num ** 3;

let num = 2
console.log(`The cube of ${num} is ${getCube(num)}.`)

let address = ["258 Washington Ave NW", "California", "90011"];
let [avenue, state, zipcode] = address;
console.log(`The full address is ${avenue}, ${state}, ${zipcode}`)

let animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    weight: "1075",
    measurement: "20 ft 3 in"
}

let { name, type, weight, measurement } = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)

const number = [1, 2, 3, 4, 5]
number.forEach(num => {
    console.log(num);
});

let reduceNumber = number.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);


class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);


