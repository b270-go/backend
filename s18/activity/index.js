/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

*/

		function numberAddition(number1, number2) {
			sum = number1 + number2;
			console.log('Displayed sum of ' + number1 + ' and ' + number2)
			console.log(sum)
		}

		function numberSubtraction(number1, number2) {
			difference = number1 - number2;
			console.log('Displayed difference of ' + number1 + ' and ' + number2)
			console.log(difference)
		}

		numberAddition(5, 15);
		numberSubtraction(20, 5);
/*

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

*/

		function numberMultiplcation(number1, number2) {
			initialProduct = number1*number2
			return(initialProduct)
		}

		function numberDivision(number1, number2) {
			initialQuotient = number1/number2
			return(initialQuotient)
		}

		globalFirstNumber = 50;
		globalSecondNumber = 10;


		product = numberMultiplcation(globalFirstNumber, globalSecondNumber)
		console.log('The product of ' + globalFirstNumber + ' and ' + globalSecondNumber + ':')
		console.log(product)

		quotient = numberDivision(globalFirstNumber, globalSecondNumber)
		console.log('The quotient of ' + globalFirstNumber + ' and ' + globalSecondNumber + ':')
		console.log(quotient)
/*

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

*/

		function areaOfACirle(radius){
			pi = 3.14
			areaMeasurement = pi * radius ** 2 
			return areaMeasurement
		}


		chosenRadius = 15
		circleArea = areaOfACirle(chosenRadius)
		console.log('The result of getting the area of a circle with ' + chosenRadius + ' radius:')
		console.log(circleArea)

/*

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

		function averageOfFourNumbers(num1, num2, num3, num4) {
			average = (num1+num2+num3+num4) / 4
			return average	
		}

		num1 = 20
		num2 = 40
		num3 = 60
		num4 = 80
		averageVar = averageOfFourNumbers(num1, num2, num3, num4)
		console.log('The average of ' + num1 + ','  + num2 + ','  + num3 + ','  + num4)
		console.log(averageVar)
/*		
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

		function scoreChecker(score, totalScore) {
			percentage = (score/totalScore)*100
			threshold = 75

			isPassed = percentage > threshold
			return(isPassed)
		}

		score = 38
		totalScore = 50
		isPassingScore = scoreChecker(score, totalScore)
		console.log('Is ' + score + '/' + totalScore + ' a passing score?')
		console.log(isPassingScore)