// console.log("Hello World!");

// ===== Arithmetic Operators ======



	let x = 10;
	let y = 12;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of product operator: " + product);

	let quotient = y / x;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);



// ====== Assignment Operators ======



	// Basic Assignment Operators(=)
	let assignmentNumber = 8;

	//Addition Assignment Operators(+-)
	// assignmentNumber = assignmentNumber + 2;
	// console.log("Result of addition assignment operator: " + assignmentNumber);

	// Shorthand for assignmentNumber = assignmentNumber + 2;
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of product assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);



// ====== Multiple Operators ======

	
	// The operations were done in the following order:\
	/* 1. 3*4
	   2. 12/5
	   3. 1+2
	   4. 3-2.4
	*/
	let number = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operator: " + number);

	let pemdas = 1 + ( 2 - 3 ) * ( 4 / 5);
	console.log("Result of pemdas operator: " + pemdas);



// ====== Type Coercion ======



	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion);
	console.log(typeof noCoercion);

	// The result is a number
	// The "true" is also associated with the value of 1 while "false" is 0
	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);



// ====== Comparison Operators =======



	let juan = "juan";


	// ====== Equality Operators (==) =======
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == "1"); //true
	console.log(0 == false); //true
	console.log("juan" == "juan"); //true
	console.log("juan" == juan); //true


	// ====== Inequality Operator (!=)
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != "1"); //false
	console.log(0 != false); //false
	console.log("juan" != "juan"); //false
	console.log("juan" != juan); //false


	// ===== Strict Equality Operator (===)
	/*
		-Checks whether the operands are equal/have the same content
		-Also compares the data types of the 2 values
		-Returns a boolean value
	*/
	console.log("Strict Equality Operator");
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === "1"); //false
	console.log(0 === false); //false
	console.log("juan" === "juan"); //true
	console.log("juan" === juan); //true


	// ====== Strict Inequality Operator (!==)
	console.log("Strict Inequality Operator");
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== "1"); //true
	console.log(0 !== false); //true
	console.log("juan" !== "juan"); //false
	console.log("juan" !== juan); //false



// ===== Relational Operators ======



	let a = 50;
	let b = 65;

	console.log("Relational Operators");

	// Greather than Operator ( > )
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false

	// Less than Operator ( < )
	let isLessThan = a < b;
	console.log(isLessThan);  //true

	// Greather than or Equal ( >= )
	let isGtOrEqual = a >= b;
	console.log(isGtOrEqual); //false

	// Less than or Equal ( <= )
	let isLtOrEqual = a <= b;
	console.log(isLtOrEqual); //true



// ====== Logical Operators =====



	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&&) - resturns true if all operands are 'true'
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);

	// Logical OR operator ( || ) - returns true if one of the operands is 'true'
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT operator ( ! ) - returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);



// ====== Increment and Decrement ===== - Operators that add or subtract values by 1 and reassign the value of the variable where the increment/decrement was applies to


	// Pre-increment - the value of 'z' is added by 1 before returning the value and storing it in the variable
	let z = 1;
	let increment = ++z;
	console.log("Results of pre=increment: " + increment);
	console.log("Results of pre-increment: " + z);

	//Post-Increment - the value of 'z' is returned first and stored in the variable 'increment' then the value of 'z' is increased by 1 
	increment = z++;
	console.log("Results of post=increment: " + increment);
	console.log("Results of post-increment: " + z);
	

	// Pre-decrement
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement)
	console.log("Result of pre-decrement: " + z)

	//post-decrement
	decrement = z --;
	console.log("Result of post-decrement: " + decrement)
	console.log("Result of post-decrement: " + z)