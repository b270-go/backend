// First task

let trainer = {
    name: "Ash Ketchum",
    age: 10, 
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoen: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },
    talk: function () {
        console.log("Pikachu! I choose you!");
    },
};

console.log(trainer)
console.log('Result of dot notation: ');
console.log(trainer.name)
console.log('Result of square bracket notation: ')
console.log(trainer["pokemon"])
console.log('Result of talk method: ')
trainer.talk()


// 2nd task

function pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    this.tackle =  function(target){
        target.health -= this.attack;
        console.log(this.name + " tackled " + target.name);
        console.log(target.name +"'s health is now reduced to " + target.health);
        if (target.health <= 0) {
            this.faint(target);
        }
    };
    this.faint = function() {
        console.log(this.name + " fainted! :(");

    }
}


let pikachu = new pokemon("Pikachu", 12);
let geodude = new pokemon("Geodude", 8);
let mewtwo = new pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude)